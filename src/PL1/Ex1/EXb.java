package PL1.Ex1;

public class EXb {
    public static int product(int n, int m) {
        if (m == 0) {
            return 0;
        }

        m--;
        return n + product(n, m);
    }

    public static void main(String[] args) {
        System.out.println(product(4, 5));
    }
}
