package PL1.Ex1;

public class EXe {
    public static void main(String[] args) {
        System.out.println(palindrome("10321"));

    }
    public static boolean palindrome(String s){
        if (s.length()==1){
            return true;
        }
        if (s.length()==2){
            return s.charAt(0) == s.charAt(1);
        }
        if (s.charAt(0) == s.charAt(s.length()-1)){
            return true && palindrome(s.substring(1,s.length()-2));
        }else{
            return false;
        }
    }
}
