package PL1.Ex1;

public class EXa {
    public static String reverse(String s) {
        if (s.length() == 1) {
            return s;
        }
        char c = s.substring(s.length() - 1).charAt(0);
        String s2 = s.substring(0,s.length() - 1);
        return c + reverse(s2);
    }

    public static void main(String[] args) {
        System.out.println(reverse("Eduardo"));
    }
}
