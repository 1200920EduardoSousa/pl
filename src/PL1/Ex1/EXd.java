package PL1.Ex1;

public class EXd {
    public static void main(String[] args) {
        System.out.println(convert("4465446"));

    }

    public static int convert(String s) {
        if (s.length()==1){
            return Integer.parseInt(s);
        }

        int i = Integer.parseInt(s.substring(0,1));

        s = s.substring(1);

        return (int) (i * Math.pow(10,s.length()) + convert(s));
    }
}
