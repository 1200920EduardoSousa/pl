package PL1.Ex1;

public class EXc {
    public static int maxdivcom(int m, int n){
        if (n == 0){
            return m;
        }
        return maxdivcom(n,m % n);
    }

    public static void main(String[] args) {

        System.out.println(maxdivcom(17,11));

    }

}
